FROM debian:jessie
MAINTAINER Jean-Mathieu Chantrein jean-mathieu.chantrein [at] univ-angers.fr

ENV TZ=Europe/Paris
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && \
    apt-get -yq install \
    makepasswd \
    vim && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
